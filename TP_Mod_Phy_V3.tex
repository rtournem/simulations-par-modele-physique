\documentclass[12pt]{report}



\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[francais]{babel}
\usepackage{graphicx}
\usepackage[left=2.5cm,right=2.5cm]{geometry}
\usepackage{color}
\usepackage{colortbl}
\usepackage{lmodern}
\usepackage{fancyhdr}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{mathtools}
\usepackage{mathrsfs}
\usepackage{multirow}
\usepackage{amsthm}
\usepackage{array}
\usepackage{float} 


\let\oldtabular=\tabular
\def\tabular{\small\oldtabular}
%\def\cleardoublepage{\hspace{1cm}}
%\def\clearpage{\hspace{1cm}}
\makeatletter
\renewcommand\chapter{%
                    \thispagestyle{plain}%
                    \global\@topnum\z@
                    \@afterindentfalse
                    \secdef\@chapter\@schapter}
\renewcommand{\@chapapp}{}
\makeatother



\newtheorem*{mydef}{}

\DeclareMathOperator{\e}{e}

\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{gray2}{rgb}{0.7,0.7,0.7}

\begin{document}


\begin{titlepage}


\begin{center}~\\[4cm]

{ \huge \bfseries Simulations temporelles par modèle physique des instruments à anche et à embouchure}\\[3cm]



\vfill

R\'edig\'e par Robin \textsc{Tournemenne} et Jean-François \textsc{Petiot}\\
{\large le 12 janvier 2016}

\end{center}
\end{titlepage}

\tableofcontents

\pagebreak
\pagestyle{headings}

%\renewcommand{\headheight}{13pt}
%\renewcommand{\headrulewidth}{1pt}
%\renewcommand{\footrulewidth}{1pt}
%\renewcommand{\headsep}{15pt}
%\voffset=-40pt
%\footskip=30pt
%\textheight = 680pt
%\lhead{\rightmark }
%\chead{}
%\rhead{}
%\lfoot{Section \thesection}
%\cfoot{Chapter \thechapter }
%\rfoot{\thepage}

\chapter{Introduction}
	\paragraph{}
	Ce TP traite de la synthèse sonore des instruments à vents.
	Il propose de traiter la famille des cuivres (non pas définis par le matériau (laiton) mais par le type d'excitateur: les lèvres : exemple la trompette) et des bois (l'excitateur : une anche, lamelle souvent en roseau, mise en vibration par la colonne d'air du musicien, exemple : la clarinette), en mettant de côté le cas des flûtes (instabilité de jet sur un biseau).
	Ces deux familles d'instruments sont régis par les mêmes phénomènes physiques, c'est pourquoi nous les appellerons instruments à anche, indépendamment de la nature de l'anche: morceau de roseau/plastique ou lèvres du musicien.
	
	\paragraph{}
	Le TP propose d'implémenter une méthode de synthèse sonore par modèle physique qui met en jeu un schéma numérique temporel.
	Sur le plan physique, il vous sera demandé de bien comprendre le fonctionnement des instruments concernés, de vous approprier le modèle physique sous-jacent, et de discrétiser une partie de ce modèle.
	Sur le plan informatique/recherche, d'implémenter le modèle numérique sur Matlab, et de synthétiser des sons afin d'analyser les forces et faiblesses de l'implémentation ainsi que les caractéristiques du modèle.
	Vous serez guidés tout au long de ces tâches par ce sujet, ainsi que par un code Matlab à trou que vous devrez compléter tout au long des quatre heures.
	Les pré-requis Matlab sont la maitrise des données telles que les vecteurs et les matrices, l'affichage de courbes, et les outils d'algorithmie standard.
	Par conséquent, \textbf{ne constituez pas de binômes ayant des difficultés en Matlab}, le reste ne devant pas poser de problèmes majeurs.
	Le texte du TP est en noir et \textbf{les questions auxquelles vous devez répondre en bleu}.
	
	\paragraph{}
	\textbf{Le compte-rendu contiendra un document réponse correspondant aux réponses du chapitre 5. 
	Lorsque le chapitre 4 sera fini, vous nous mettrez de côté votre code Matlab afin de nous l'envoyer tel quel avec le compte-rendu, avec certains sons caractéristiques.}

\chapter{Physique de l'instrument}
	\section{Fonctionnement des instruments à anche}
		\subsection{Anatomie de la trompette et de la clarinette}
			\begin{figure}[h]
				\centering
				\textbf{Anatomie de la trompette}\par\medskip
				\includegraphics[scale=2]{Pictures/TRUMPET.pdf}
				\caption{	Définition des parties principales de la trompette: l'embouchure en gris, la branche d'embouchure en noir, le cylindre coudé, les pistons et le pavillon en jaune.}
				\label{trump}
			\end{figure}
			\paragraph{}
			Cet instrument cylindro-conique est constitué de quatre parties que nous présentons en partant des lèvres: 
			\begin{description}
				\item[l'embouchure:] partie amovible d'environ 8cm de long consituée d'une partie hémisphérique où le musicien pose ses lèvres et un cône lentement divergeant qui vient rattraper le rayon d'entrée de l'instrument,
				\item[la branche d'embouchure:] premier tube conique très peu divergeant d'environ 22cm de long et 5mm de rayon que le facteur d'instrument utilise pour ajuster la justesse de l'instrument,
				\item[le cylindre central et les pistons:] cylindre coudé de 50cm de long qui peut s'allonger grâce à l'action des doigts sur les pistons,
				\item[le pavillon:] partie fortement conique permettant de rayonner le son et également d'adapter la justesse).
			\end{description}
									
			\begin{figure}[ht]
				\centering
				\textbf{Anatomie de la clarinette et vue en coupe du bec}\medskip
				
				\includegraphics[scale=0.5]{Pictures/clarinette.eps}\linebreak
				\hspace*{2cm}    
				\includegraphics[scale=0.5]{Pictures/bec.pdf}\linebreak
				\caption{Au dessus, définition des parties principales de la clarinette: le bec en bleu, le barillet en rouge, le corps et les clés en noir, le pavillon en vert. En dessous, vue en coupe du bec.}
				\label{clar}
			\end{figure}

			\paragraph{}
			Cet instrument cylindrique est constitué de quatre parties:
			\begin{description}
				\item[le bec:] partie amovible pour fixer l'anche avec une ligature,
				\item[le barillet:] tube cylindrique permettant au musicien d'accorder son instrument,
				\item[le corps:] cylindre dont la longueur acoustique utile varie en fonction du dernier trou bouché par les clés sous l'action des doigts du musicien,
				\item[le pavillon:] partie légèrement conique permettant de rayonner le son et accessoirement de filtrer le son (passe haut).
			\end{description}
		
			\paragraph{}	
			\textbf{les deux différences majeures entre la trompette et la clarinette sont:	}
			\begin{itemize}
				\item la clarinette est considérée comme conique et la trompette comme cylindro-conique
				\item la clarinette fonctionne par l'intermédiaire d'une anche, alors que la trompette fonctionne par l'intermédiaire des lèvres du musicien 
			\end{itemize}
		
		\subsection{Fonctionnement général et impédance d'entrée}
		
			\paragraph{}
			Le fonctionnement des instruments à anche où à embouchure est similaire au détail près qu'il faille remplacer le mot anche par lèvre. 
			C'est pourquoi nous nous permettons de reprendre entièrement la description du fonctionnement de la trompette issu d'un article de vulgarisation \cite{Joel} (qui se trouve aussi en intégralité dans les fichiers transmis pour ce TP).
							
			\paragraph{}
			"Au premier abord, on peut penser que le matériau est la cause principale [des] qualités sonores [de la trompette]. 
			Or les recherches en acoustique ont montré, au cours du XXe siècle, qu'il ne joue qu'un rôle secondaire sur le son produit. 
			En adaptant une embouchure de trombone sur un tuyau de plastique de plusieurs mètres, un instrumentiste expérimenté obtient un son "cuivré" semblable à celui d'un instrument !
			
			\paragraph{}
			En réalité, un cuivre se définit, non par son matériau, mais par la façon dont le son est émis : la colonne d'air à l'intérieur de l'instrument - la perce - est mise en résonance par la vibration des lèvres du musicien. 
			Aussi classe-t-on parmi les cuivres des instruments non métalliques, telle la conque marine, ou réalisés en bois, comme le cornet à bouquin et le serpent. 
			Le laiton utilisé généralement dans les cuivres possède cependant des qualités intéressantes : il est notamment très ductile, donc relativement facile à mettre en forme. 
			Lisse et non poreux, il permet d'obtenir une surface d'excellente qualité.
			
			\paragraph{}
			La perce se définit mathématiquement comme l'évolution du rayon intérieur de l'instrument en fonction de la ligne moyenne allant de l'embouchure au pavillon. 
			Elle a une très grande influence sur le comportement de l'instrument (justesse et timbre). 
			Indépendamment des qualités esthétiques de l'instrument, le fabricant de cuivres - le facteur - doit donc surtout ajuster la perce en fonction du résultat musical désiré.
			
			\paragraph{}				
			[...]
			
			\paragraph{}
			Reprenons notre trompette. 
			Son registre courant - les notes couvertes du grave à l'aigu - s'étend sur deux octaves et demie, du fa dièse grave au do au-dessus de la portée. 
			En faisant varier la conformation de ses lèvres, son "masque", et son souffle dans l'embouchure, le musicien expérimenté obtient, pour un même doigté sur les trois pistons de l'instrument, des notes nommées partiels. 
			Elles sont proches d'une série harmonique, c'est-à-dire proches d'une suite de fréquences multiples d'une fréquence fondamentale.
			
			\paragraph{}
			La colonne d'air intérieure est excitée et mise en résonance par les vibrations des lèvres du musicien, une succession rapide d'ouvertures et de fermetures de la bouche. La maîtrise de cette technique, le buzz (le bourdonnement), nécessite des années de pratique. 
			Lorsque le musicien y parvient, la pression variable que son buzz exerce dans l'embouchure excite la colonne d'air dans l'instrument ; celle-ci se met à vibrer, puis rétroagit sur les lèvres, qui excitent à nouveau la colonne d'air, jusqu'à l'obtention d'un son stable. 
			En configurant son masque, le musicien produit l'un des partiels correspondant à un doigté particulier. 
			Les physiciens associent d'ailleurs souvent les cuivres et les instruments à anche (clarinette, saxophone, hautbois, basson, etc.), car le buzz a le même rôle que la vibration de l'anche : moduler le souffle du musicien, et produire un son par "effet valve".
			
			\paragraph{}
			Les cuivres fonctionnent donc grâce au couplage entre les lèvres du musicien, le système excitateur, et la colonne d'air contenue dans l'instrument, le résonateur, qui propage et amplifie l'onde sonore émise. 
			La première caractéristique du résonateur est son impédance d'entrée acoustique : l'amplitude de sa réponse à une excitation donnée ; autrement dit, le rapport, à l'entrée de l'instrument, entre la pression acoustique, la variation de pression par rapport à la pression atmosphérique, et le débit acoustique, c'est-à-dire la variation de débit par rapport au débit moyen. Lorsque l'on mesure l'impédance, on constate que son amplitude atteint un maximum pour plusieurs fréquences particulières, les fréquences de résonance (voir la figure \ref{imped}). 
			Ces dernières corespondent aux partiels les plus faciles à émettre avec l’instrument."
			
			\begin{figure}[h]
				\centering
				\textbf{Impédance d'entrée}\par\medskip
				\includegraphics[scale=0.2]{Pictures/impedance.pdf}
				\caption{	Impédance d'entrée typique d'une trompette en si bémol (amplitude), montrant les résonances 2, 3, 4 et 5 de l'instrument.}
				\label{imped}
			\end{figure}

			\paragraph{}			
			Ainsi, le modèle physique se décompose en deux parties, l'excitateur et le résonateur avec une boucle de rétroaction entre le résonateur et l'excitateur. L'excitateur (les lèvres du musicien, l'anche de la clarinette) est fortement non-lnéaire alors que le résonateur (l'instrument) est linéaire (voir Figure \ref{process} traduite de \cite{mcintyre1983oscillations}). Cette figure résume d'ailleurs le fonctionnement de tous les instruments fonctionnant en auto-oscillation (vents, cordes).
				
			\begin{figure}[h]
				\centering
				\textbf{Phénomène physique}\par\medskip
				\includegraphics[scale=0.2]{Pictures/Process.pdf}
				\caption{ Sous l'action d'une source d'énergie le musicien (élément non-linéaire, caractérisé par une fonction NL) excite le résonateur (élément linéaire) qui est couplé au musicien par la boucle de rétroaction.}
				\label{process}
			\end{figure}
				
			
			
	\section{Modèle physique et paramètres musicien}

		\subsection{Modèle physique}
			
			\paragraph{}
			Le modèle physique classique des instruments à embouchure présent dans la littérature est donné par trois équations qui représentent le comportement des différentes parties du système \cite{campbellGeneral}. Celles-ci mettent en jeu trois grandeurs physiques variables dans le temps : l'ouverture inter-lèvre (ou la hauteur de l'anche $H(t)$), le débit acoustique $u(t)$ et la pression $p(t)$ dans l'embouchure (Figure \ref{model}).
			\begin{equation}
			\label{instru}
  				p(j\omega)=Z(j\omega)u(j\omega),
			\end{equation}
			\begin{equation}
				\label{lips}
				\frac{d^2H(t)}{dt^2}+\frac{2\pi f_r}{Q_r}\frac{dH(t)}{dt}+(2\pi f_r)^2(H(t)-H_0)=\frac{P_m-p(t)}{\mu_r},
			\end{equation}
			\begin{equation}
				\label{mouth}
				\text{and} ~~~~ u(t)=\Theta(H(t)) \ bH(t)sgn(P_m-p(t))\sqrt{\frac{2|P_m-p(t)|}{\rho}}.
			\end{equation}
			
				L'Équation (\ref{instru}), définie dans le domaine fréquentiel, décrit le comportement acoustique du résonateur.
			Elle représente l'impédance $Z$ de l'instrument, défini comme le ratio entre la pression acoustique dans l'embouchure $p(j\omega)$ et le débit acoustique  $u(j\omega)$ entrant dans l'instrument.
			
			Les Équations (\ref{lips}) et (\ref{mouth}) décrivent le comportement de l'excitateur:
			\begin{itemize}
				\item l'Équation (\ref{lips}) décrit le comportement mécanique des lèvres du musicien considérées comme un oscillateur mécanique à un degré de liberté, le degré de liberté étant l'ouverture inter-lèvre ou la hauteur de l'anche $H(t)$ ($u(t)=0$ est imposé si $H(t)<0$),
				\item l'Équation (\ref{mouth}) obtenue à partir du théorème de Bernoulli, représente un couplage non linéaire entre la pression dans l'embouchure $p(t)$, $h(t)$ et le débit acoustique $u(t)$. $\Theta$ est la fonction Heavyside afin de respecter la contrainte $u(t)=0$ si $H(t)<0$.
			\end{itemize}			

			Le modèle présenté (équations (\ref{instru}), (\ref{lips}) et (\ref{mouth})) est celui correspondant à un cuivre (par ex. la trompette). En effet, pour ce type d'instrument, l'augmentation de pression dans la bouche produit une ouverture des lèvres - notez ainsi dans l'équation (\ref{lips}) que $P_m-p(t)$>0 conduit à une augmentation de $H(t)$).
			Pour utiliser ce modèle avec la clarinette et les instruments à anche en général, il suffit de prendre l'opposé de la différence dans  l'Équation (\ref{lips}) c'est à dire de remplacer $P_m-p(t)$ par $p(t)-P_m$ (l'augmentation de pression dans la bouche conduit à une fermeture de l'anche).
			
			\paragraph{}


			
			\begin{figure}[h]
				\centering
				\textbf{schématisation du modèle physique}\par\medskip
				\includegraphics[scale=1]{Pictures/model.pdf}
				\caption{Représentation du modèle physique.
					La ligne de courant symbolise la relation de Bernoulli entre $P_m$ , $p$ et $\frac{u}{S}$.
					L'oscillateur à une masse, représenté par la masse rectangulaire, lie $H$ à $p$ et $P_m$.
					L'instrument caractérisé par son impédance d'entrée $Z$ lie $p$ à $u$.}
				\label{model}
			\end{figure}	
		
			Bien que des modèles plus compliqués puissent être construits en considérant des propriétés physiques plus fines, ce modèle basique est suffisant pour caractériser la physique sous-jacente aux instruments à anche et à embouchure et simuler des sons de manière représentative.
		\subsection{Paramètres musiciens}
		
			\paragraph{}
			Afin de produire des simulations sonores, il est nécessaire de définir des valeurs cohérentes (c'est à dire des valeurs qui permettent d'atteindre un régime permanent stable pour une note donnée) pour les paramètres du masque du musicien (en d'autres termes, il est important "d'apprendre" à l'ordinateur comment jouer de l'instrument). 
			Dans notre modèle, il y a 6 paramètres liés au musicien:
			
			\begin{table}[h!]
				\caption{Paramètres de contrôle du musicien ("masque"). Le mot anche peut être remplacé par lèvres selon l'instrument considéré.)}
				\label{param}
				\center
				\begin{tabular}{lcc}
				Définition                      & Notation   \\ \hline
					fréquence de résonance de l'"anche" &  $f_r$ (Hz)    \\ 
					masse surfacique de l'"anche"       & $\mu_r$ ($kg/m^2$)\\  
					Pression dans la bouche           & $P_m$ (kPa)      \\
					Largeur de l'"anche"               & $b$ (mm)           \\
					Hauteur de repos de l'"anche" & $H_0$ (mm)         \\
					Facteur de qualité  & $Q_r$           
				\end{tabular}
			\end{table}    
						
			Deux jeux de valeurs selon que vous simuliez le son d'une trompette ou d'une clarinette simplifiée sont à disposition au début du fichier MainSimuRepImp.m. Ils devront être utilisés pour confirmer que l'algorithme fonctionne avant d'expérimenter différents paramètres.
								
\chapter{Simulations par modèle physique}
	
	\section{Notions générales}
		
		\paragraph{}
		Le modèle étant fixé et les paramètres explicités, il faut maintenant exposer comment les simulations sonores seront obtenues.
		La premier choix concerne le type de simulations souhaités. Il en existe deux types: 
		\begin{itemize}
			\item simulation fréquentielle: on recherche les amplitudes et phases d'une décomposition en série de Fourier du signal recherché. On simule alors le son en régime permanent,
			\item simulation temporelle: selon une certaine fréquence d'échantillonnage $F_e$ on va déterminer les valeurs de tous les échantillons de la pression (un schéma numérique permettra de calculer la valeur de l'échantillon suivant à partir des échantillons précédents selon l'axe temporel).
		\end{itemize}
		
		\paragraph{}
		Les simulations fréquentielles sont particulièrement adaptées au système linéaire car ces systèmes respectent le théorème de superposition ce qui sied particulièrement aux séries de Fourier. 
		Notre système étant non-linéaire à cause de l'excitateur, les méthodes de simulations fréquentielles sont donc assez complexes (décomposition en fonction puissance, équilibrage harmonique). 
		Nous utiliserons tout de même les résultats d'une simulation fréquentielle sur notre trompette d'étude afin de bien vérifier la fiabilité de nos simulations temporelles.

		\paragraph{}	
		Nous privilégierons donc des simulations temporelles pour ce TP.
		\textbf{Le but est de déterminer les trois suites $( p(nT_e))_{n\in \mathbb{Z}}$, $( u(nT_e))_{n\in \mathbb{Z}}$ et $( y(nT_e))_{n\in \mathbb{Z}}$, $T_e$ étant la période d'échantillonnage. Par souci de simplicité, $T_e$ sera omis par la suite et nous parlerons de $p[n]$ pour parler de l'échantillon de pression à l'instant $nT_e$.}
		
		\paragraph{}
		La difficultés majeure inhérente à ce type de simulation est la \textbf{boucle sans retard}.
		En effet, de toute évidence, à l'instant $t$ on ne peut utiliser que les informations passées pour déterminer la valeur de l'échantillon courant.
		Si une équation fait intervenir deux grandeurs inconnues devant être déterminées simultanément, on obtient donc une boucle sans retard et la création d'un schéma numérique est impossible.

		\paragraph{}		
		En terme mathématique, les équations dites implicites doivent être explicités d'une manière ou d'une autre.
		En terme de traitement du signal, on doit supprimer les transmissions directes des fonctions de transfert.
		 
		\paragraph{}				 
		Les relations instantanées entre deux grandeurs inconnues sont donc un problème.
		Par exemple, notre Équation (\ref{mouth}) est instantanée: pour connaître le débit acoustique $v$ à l'instant $t$ il faut connaître $p$ et $H$ au même moment!		
		 Par ailleurs, les produits de convolution faisant intervenir une intégrale sur tout $\mathbb{R}$ sont généralement un problème (définition temporelle de notre résonateur).
		 
		\paragraph{}				 			
		L'objectif des paragraphes suivants sera donc de discrétiser les équations de notre modèle continu afin d'obtenir des formules récurrentes pour les trois grandeurs $p(t)$, $u(t)$ et $H(t)$.
		
	\section{Étude du modèle et stratégie employée}
	
		\subsection{représentation temporelle de l'instrument}
		
			\paragraph{}
			L'équation décrivant le comportement de l'instrument est: 
			$$
  				p(j\omega)=Z(j\omega)u(j\omega).
			$$
			
			Dans le domaine temporel, la pression est obtenue à partir de la convolution entre la réponse impulsionnelle du système et le débit acoustique. 
			La réponse impulsionnelle est la réponse du système à une impulsion de Dirac (voir Figure \ref{repimp}).
			
			\begin{figure}[h]
				\centering
				\textbf{Réponse impulsionnelle de trompette}\par\medskip
				\includegraphics[scale=0.3]{Pictures/repimp.pdf}
				\caption{Réponse impulsionnelle de la trompette du TP avec continuation haute fréquence.}
				\label{repimp}
			\end{figure}	
			
			
			Mathématiquement, c'est simplement la transformée inverse de l'impédance d'entrée:
			$$
				g=FFT^{-1}(Z)
			$$
			$$
				p(t)=(g*u)(t)
			$$
			
			La convolution de suites discrètes est donnée par:
			$$
				p[n]=\sum_{k=-\infty}^{+\infty}{g[k]u[n-k]}.
			$$
			La réponse impulsionnelle étant causale et négligeable après un certain échantillon $N_{tronc}$ (voir Figure \ref{repimp}) l'équation précédente devient:
			$$
				p[n]=\sum_{k=0}^{N_{tronc}}{g[k]u[n-k]}.
			$$
			Ce que l'on peut réécrire:
			$$
				p[n]=g[0]u[n]+V, \qquad V=\sum_{k=0}^{N_{tronc}-1}{g[k+1]u[n-1-k]}.
			$$
			$V$ ne dépend que du passé de $(u(n))$.
			On voit donc apparaitre une relation implicite entre $p$ et $v$ par l'intermédiaire de $g[0]$: il faut avoir l'échantillon $u[n]$ afin d'obtenir $p[n]$.
			
		\subsection{dynamique de l'anche/embouchure}
		
			\paragraph{}
			Afin d'implémenter facilement l'algorithme, nous nous baserons non pas sur la suite $(H(n))$ mais sur la suite $(y(n))$ qui est l'écart à la position de repos:
			$$
				y=H-H_0
			$$
			L'Équation (\ref{lips}) se réécrit donc:
			$$
				\frac{d^2y(t)}{dt^2}+\frac{2\pi f_r}{Q_r}\frac{dy(t)}{dt}+(2\pi f_r)^2y(t)=\frac{P_m-p(t)}{\mu_r}.
			$$
			D'après \cite{guillemain2005real}, il est tout à fait acceptable d'approcher cette équation par l'approximation centrée des dérivées:
			$$
				\frac{dy(nT_e)}{dt}=\frac{y[n+1]-y[n-1]}{2T_e}
			$$
			$$
				\frac{d^2y(nT_e)}{dt^2}=\frac{y[n+1]-2y[n]+y[n-1]}{T^{2}_{e}}
			$$
			
			{\color{blue} \textbf{Déterminez la relation récurrente entre les suites $(y(n))$ et $(p(n))$.}
			
			\paragraph{}
			L'équation est-elle implicite? 
			}
			
			
		\subsection{résolution des équation implicites}
		
			\paragraph{}
			La relation entre le résonateur et l'équation instantanée obtenue à partir de la relation de Bernoulli sont implicites:
			$$
				\left\{\begin{matrix*}[l]
					p[n]=g[0]u[n]+V\\ 
					u[n]=\Theta(H[n]) \ b(y[n]+H_0) sgn(P_m-p[n]) \sqrt{\frac{2|P_m-p[n]|}{\rho}}\\ 
					g[0]>0
				\end{matrix*}\right.
			$$
			
			{\color{blue} 
			\textbf{En supposant initialement le signe de $P_m-p[n]$, combinez ces deux équations pour résoudre en $u[n]$ puis en regroupant tous les termes connus à l'instant $nT_e$ sour la lettre $W$ étudiez l'équation du second degré obtenue.}
			
			Enfin, déterminez l'expression explicite de $(u(n))$ et $(p(n))$.
			}
			
			\paragraph{}
			\textbf{La méthode que vous venez de développer a été publiée par Phillipe Guillemain, Jean Kergomard et Thierry Voinier en 2005 pour faire de la simulation temporelle par modèle physique en temps réel \cite{guillemain2005real}.}
				
\chapter{Implémentation numérique: Matlab}

	\section{Présentation}
		Disposant maintenant d'un schéma numérique constitué de trois équations récurrentes permettant de déterminer les suites $(p(n))$, $(u(n))$ et $(y(n))$, nous allons pouvoir compléter le code à trou Matlab.
		
		\paragraph{}
		\textbf{Les paragraphes suivants décrivent les tâches demandées, mais le code Matlab est lui-même commenté ce qui vous permettra de lever vos incertitudes la plupart du temps.}
	
		\paragraph{}	
		Le code Matlab se lance par l'intermédiaire du fichier MainSimuRepImp.m. Ce fichier définit les paramètres musicien, récupère la réponse impulsionnelle de la clarinette ou de la trompette selon l'instrument choisi, calcul par l'intermédiaire d'une boucle for les suites  $(p(n))$, $(u(n))$ et $(y(n))$ et analyse le son produit. La Figure \ref{struct} schématise la structure du programme.
	
		\begin{figure}[H]
			\centering
			\textbf{Structure du programme}\par\medskip
			\includegraphics[scale=0.45]{Pictures/struct.pdf}
			\caption{Schéma décrivant le programme. La partie de gauche représente la boucle avec les trois formules de récurrence et la partie de droite l'extraction des réponses impulsionnelles.}
			\label{struct}
		\end{figure}	
					
	\section{Algorithme}
			
			\paragraph{}
			Il ne nous reste plus qu'à retranscrire dans Matlab les équations trouvées sur le papier.
			Les deux fonctions à modifier sont oneMassDis.m et ImplicitFunc.m.
			La première contient la formule récurrente calculant $(y(n))$, la seconde calcule $V$ par la sous-fonction PastOfRes.m puis (u(n)) et (p(n)). 
			
			{\color{blue} 
			
			\paragraph{}
			Avant toute chose, complétez le vecteur $t$ définissant l'axe temporel (ligne 44 de MainSimuRepImp.m). Ensuite, initialisez les vecteurs y, p et u afin que les formules récurrentes démarrent en sachant que tous ces signaux sont causaux (égaux à 0 pour tout $t$ inférieur à 0)  (ligne 45, 46, 47 de MainSimuRepImp.m).

			\paragraph{}
			Vous commencerez par remplir la fonction oneMassDis en créant bien quatre coefficients contenant $w_r, Q_r, f_e, \mu_r$, puis une formule récurrente simple et \textbf{lisible}.
			
			\textbf{ATTENTION}, selon que l'on traite la clarinette ou la trompette le signe de $P_m-p[n]$ change.
			C'est le paramètre Param.Cyl (début de MainSimuRepImp.m) qui choisit si vous voulez simuler la clarinette ou la trompette.

			\paragraph{}		
			Pour finir, dans la fonction ImplicitFunc, définissez $W$ puis recopiez la formule récurrente de $u$ (récurrente à cause de $V$). Le vecteur contenant la réponse impulsionnelle est la variable Gimp)
			
			\paragraph{}
			Croisez les doigts et lancez le code complet. Si vous n'obtenez pas les signaux de la Figure \ref{SonClarinette} (le son de clarinette qu'il fallait obtenir), vous avez fait une erreur dans les coefficients quelque part. 
			Essayez de débogguer en contrôlant vos coefficients au cours des itérations et, si vous avez toujours le même problème, venez nous voir et nous jetterons un oeil.
			}

			\begin{figure}[H]
				\centering
				\textbf{Son de clarinette}\par\medskip
				\includegraphics[scale=0.8]{Pictures/clar.png}
				\caption{De haut en bas, p(t), u(t) et y(t)}
				\label{SonClarinette}
			\end{figure}
			
			{\color{blue} 
			
			\paragraph{}
			Maintenant que les simulations numériques fonctionnent, essayez les paramètres de la trompette (décommentez les lignes 17 à 24 du fichier MainSimuRepImp.m). 
			
			\textbf{Une fois que tout fonctionne, avant de passer aux paragraphes suivants, enregistrez le code matlab et faites en une copie que vous nous enverrez avec le compte-rendu afin que nous contrôlions votre travail jusqu'à ce point.}
			}

			
\chapter{Observation du phénomène physique}

	Nous allons pouvoir étudier le modèle et estimer dans quelles mesures les simulations retranscrivent les phénomènes physiques mis en oeuvre dans les instruments à anche.
	
	\section{spectre des sons}
		{\color{blue}
		
		\begin{itemize}
			\item Le spectre de la clarinette vous semble t-il correct ? (les spectres d'instrument cylindrique ont normalement une caractéristique singulière)
			\item Le spectre de trompette obtenu par notre méthode peut être comparé avec celui obtenu par une méthode fréquentielle dite d'équilibrage harmonique. 
			Le spectre obtenu par l'équilibrage harmonique est le graphique final contenant des barres verticales bleues correspondant à chaque partiel du son (la fréquence de jeu obtenue par l'équilibrage harmonique est 470.1Hz).
			Ce graphe correspond il exactement à votre analyse fft?
			
			Cet écart plutôt léger puisque l'on garde le même profil peut être dû à la condition de lèvres battantes implémentée différemment avec la méthode d'équilibrage harmonique et surtout à cause de l'usage de la réponse impulsionnelle qui prend en compte toute l'impédance contrairement à l'équilibrage harmonique qui peut se contenter uniquement des pics de l'impédance.
			 
		\end{itemize}
		}
	
	\paragraph{}
	\textbf{Les questions suivantes sont posées pour les deux instruments. Si vous n'avez pas le temps, ne traitez que la trompette.}	
	
	\section{influence de la fréquence d'échantillonnage}
		{\color{blue} Existe-t'il une fréquence d'échantillonnage minimale en dessous de laquelle notre algorithme devient instable? Cela pose-t'il un problème?
		 }
	\section{pression de seuil}
	
		\paragraph{}
		Un musicien qui soufflerait trop faiblement dans l'instrument ne produirait aucun son. 
		
		\paragraph{}
		{\color{blue} 
		Trouvez-vous une pression dans la bouche $P_m$ de seuil en dessous de laquelle le son produit est evanescent?
		 }
		 		
	\section{la dynamique de l'anche}
		\subsection{influence de $Q_r$}
		
		\paragraph{}
		Plus $Q_r$ est petit plus l'amortissement est fort et vice-versa.
		
		\paragraph{}
		{\color{blue} 
		Étudiez l'influence de $Q_r$
		 }				
			
		\subsection{influence $mu_r$}
		
			\paragraph{}
			L'expérience montre que le modèle devient instable lorsque $\mu_r$ devient trop petit.			

			\paragraph{}
			{\color{blue} 
			Q'observez-vous?
			}
						
		\subsection{influence $f_r$}
		
			\paragraph{}
			Fletcher a montré que pour un modèle de trompette, la fréquence de jeu était supérieure à la fréquence de résonance de l'impédance, elle-même supérieure à la fréquence de résonance des lèvres \cite{fletcher1979excitation}.
			Cette série d'inégalité est opposée dans le cas d'une clarinette ($f_{jeu}<f_{res}<f_r$).

			\paragraph{}
			{\color{blue} 			
			Observez-vous cette caractéristique?
			}  

			\paragraph{}
			Par ailleurs, c'est ce paramètre qui décide quelle note sera jouée. 

			\paragraph{}
			{\color{blue} 						
			Réussissez-vous à produire d'autres notes ?(commencez par la trompette)
			}

	\section{variation des paramètres au cours du temps}
		
		Après cette prise en main approfondie, vous pouvez maintenant profiter des simulations temporelles à leur maximum: implémentation de coefficients non constant et faire de la musique...
		
		En effet vous pouvez modifier au cours du temps les 6 paramètres musiciens afin d'observer l'impact de ces changements sur notre système dynamique.
		
		Par exemple, informatiquement, au lieu de travailler à $P_m$ constant, on peut modifier sa valeur pour chaque itération de la boucle for. 
		Pour cela, il faut déclarer un vecteur $P_m$ de la même taille que le vecteur $t$ au début de MainSimuRepImp.m et actualiser la valeur de Param.Pm à chaque itération.
		
		{\color{blue} 
			Seulement pour les simulations de trompette.				
			Implémentez un son de deux secondes qui de 0 à 0.5 seconde fait passer $P_m$ de 0 à 5000Pa linéairement puis après un plateau de 1.5 seconde revient à 0Pa linéairement (crescendo, decrescendo).
			Dans une autre simulation faite varier linéairement de 0 à 2 seconde $f_r$ de 380 à 450Hz.
		}
		
		

\bibliographystyle{plain}
\bibliography{biblio_ModPhy}

		
\end{document}
