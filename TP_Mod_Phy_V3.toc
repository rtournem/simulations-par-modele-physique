\select@language {french}
\contentsline {chapter}{\numberline {1}Introduction}{3}
\contentsline {paragraph}{}{3}
\contentsline {paragraph}{}{3}
\contentsline {paragraph}{}{3}
\contentsline {chapter}{\numberline {2}Physique de l'instrument}{4}
\contentsline {section}{\numberline {2.1}Fonctionnement des instruments \IeC {\`a} anche}{4}
\contentsline {subsection}{\numberline {2.1.1}Anatomie de la trompette et de la clarinette}{4}
\contentsline {paragraph}{}{4}
\contentsline {paragraph}{}{5}
\contentsline {paragraph}{}{6}
\contentsline {subsection}{\numberline {2.1.2}Fonctionnement g\IeC {\'e}n\IeC {\'e}ral et imp\IeC {\'e}dance d'entr\IeC {\'e}e}{6}
\contentsline {paragraph}{}{6}
\contentsline {paragraph}{}{6}
\contentsline {paragraph}{}{6}
\contentsline {paragraph}{}{6}
\contentsline {paragraph}{}{7}
\contentsline {paragraph}{}{7}
\contentsline {paragraph}{}{7}
\contentsline {paragraph}{}{7}
\contentsline {paragraph}{}{7}
\contentsline {section}{\numberline {2.2}Mod\IeC {\`e}le physique et param\IeC {\`e}tres musicien}{8}
\contentsline {subsection}{\numberline {2.2.1}Mod\IeC {\`e}le physique}{8}
\contentsline {paragraph}{}{8}
\contentsline {paragraph}{}{9}
\contentsline {subsection}{\numberline {2.2.2}Param\IeC {\`e}tres musiciens}{9}
\contentsline {paragraph}{}{9}
\contentsline {chapter}{\numberline {3}Simulations par mod\IeC {\`e}le physique}{11}
\contentsline {section}{\numberline {3.1}Notions g\IeC {\'e}n\IeC {\'e}rales}{11}
\contentsline {paragraph}{}{11}
\contentsline {paragraph}{}{11}
\contentsline {paragraph}{}{11}
\contentsline {paragraph}{}{12}
\contentsline {paragraph}{}{12}
\contentsline {paragraph}{}{12}
\contentsline {paragraph}{}{12}
\contentsline {section}{\numberline {3.2}\IeC {\'E}tude du mod\IeC {\`e}le et strat\IeC {\'e}gie employ\IeC {\'e}e}{12}
\contentsline {subsection}{\numberline {3.2.1}repr\IeC {\'e}sentation temporelle de l'instrument}{12}
\contentsline {paragraph}{}{12}
\contentsline {subsection}{\numberline {3.2.2}dynamique de l'anche/embouchure}{14}
\contentsline {paragraph}{}{14}
\contentsline {paragraph}{}{14}
\contentsline {subsection}{\numberline {3.2.3}r\IeC {\'e}solution des \IeC {\'e}quation implicites}{14}
\contentsline {paragraph}{}{14}
\contentsline {paragraph}{}{15}
\contentsline {chapter}{\numberline {4}Impl\IeC {\'e}mentation num\IeC {\'e}rique: Matlab}{15}
\contentsline {section}{\numberline {4.1}Pr\IeC {\'e}sentation}{15}
\contentsline {paragraph}{}{15}
\contentsline {paragraph}{}{15}
\contentsline {section}{\numberline {4.2}Algorithme}{17}
\contentsline {paragraph}{}{17}
\contentsline {paragraph}{}{17}
\contentsline {paragraph}{}{17}
\contentsline {paragraph}{}{17}
\contentsline {paragraph}{}{17}
\contentsline {paragraph}{}{18}
\contentsline {chapter}{\numberline {5}Observation du ph\IeC {\'e}nom\IeC {\`e}ne physique}{19}
\contentsline {section}{\numberline {5.1}spectre des sons}{19}
\contentsline {paragraph}{}{19}
\contentsline {section}{\numberline {5.2}influence de la fr\IeC {\'e}quence d'\IeC {\'e}chantillonnage}{19}
\contentsline {section}{\numberline {5.3}pression de seuil}{20}
\contentsline {paragraph}{}{20}
\contentsline {paragraph}{}{20}
\contentsline {section}{\numberline {5.4}la dynamique de l'anche}{20}
\contentsline {subsection}{\numberline {5.4.1}influence de $Q_r$}{20}
\contentsline {paragraph}{}{20}
\contentsline {paragraph}{}{20}
\contentsline {subsection}{\numberline {5.4.2}influence $mu_r$}{20}
\contentsline {paragraph}{}{20}
\contentsline {paragraph}{}{20}
\contentsline {subsection}{\numberline {5.4.3}influence $f_r$}{20}
\contentsline {paragraph}{}{20}
\contentsline {paragraph}{}{20}
\contentsline {paragraph}{}{20}
\contentsline {paragraph}{}{20}
\contentsline {section}{\numberline {5.5}variation des param\IeC {\`e}tres au cours du temps}{21}
